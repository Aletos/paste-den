<?php
    //Make sure they use SSL
    /*if ($_SERVER['SERVER_PORT'] != 443) {
        header("HTTP/1.1 301 Moved Permanently");

        header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
        exit();

    }*/

    session_start();

    //libraries
	require_once 'libraries/htmlpurifier-4.4.0/library/HTMLPurifier.auto.php';
    
    //objects
    require_once("objects/DB.php");
    require_once("objects/User.php");
    require_once("objects/Task.php");
    require_once("objects/Paste.php");
    require_once("objects/Topic.php");


    //views
    require_once("views/Layout.php");
    require_once("views/UserView.php");

    //utilities
	require_once('utilities.php');
?>
