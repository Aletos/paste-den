<?php
    include_once("../nullicon_namespace.php");
    
    $body = "";
    $username = "";
    $year = "";
    $major = "";
    $password1 = "";
    $password2 = "";
    $message = "";
    
    if(isset($_POST['username']) && isset($_POST['password1'])
            && isset($_POST['year']) & isset($_POST['major']) && isset($_POST['password2']))
    {
        $username = utils::getPureString($_POST, 'username');
        $year = utils::getPureString($_POST, 'year');
        $major = utils::getPureString($_POST, 'major');
        $password1 = utils::getPureString($_POST, 'password1');
        $password2 = utils::getPureString($_POST, 'password2');


        if(strcmp($password1, $password2))
        {
            $message = "Passwords don't match.";
            
        }
        else if(strlen($password1)< 8)
        {
            $message = "Password must be at least 8 characters";

        }
        else if(strlen($username) <= 0)
        {
            $message = "Username is required";
        }
        else
        {
            $db = new DB();
            $user = new User();
            $user->username = $username;
            $user->year = $year;
            $user->major = $major;
            $user->salt = utils::randString(20);
            $user->password = hash("sha512", $user->salt . $password1);

            //Check if the username is already in the db
            $criteria = array ( "username" => (string) $username);
            $old_users = $db->getList("users", $criteria, "User");
            if(count($old_users) > 0)
            {
                $message = "$username is already taken";
                $username = "";
            }
            else
            {
                $db->save("users", $user );
                $_SESSION['user_id'] = $user->_id;
                Header("Location: list.php");
            }

            

        }
        
    }
    
?>
<html>
    <?php include("../head.php"); ?>
    <body>
        <?php
        echo $message . "<br/>";
        echo "<form action='register.php' method='post'>
                <input type='text' placeholder='Username' value='$username' name='username'/>
                <input type='text' placeholder='Year'/ value='$year' name='year'>
                <input type='text' placeholder='Major'/ value='$major' name='major'>
                <input type='password' placeholder='Password' name='password1'/>
                <input type='password' placeholder='Confirm Password'  name='password2'/>
                <input type='submit'/>
            </form>";
        ?>
    </body>
    <?php include("../footer.php"); ?>
</html>
