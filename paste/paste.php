<?php
	include_once("../nullicon_namespace.php");
	$user = utils::getLogin();
	$db = new DB();
	//$message = utils::getPureString($_POST, 'message');
	$topic_id = utils::getPureString($_POST, 'topic_id', null);
	//$topic_id = $_POST['topic_id'];
	$message = $_POST['message'];
	echo "$message";
	if($message || isset($_FILES["fileupload"]['name']) )
	{
		$paste = new Paste();
		$paste->message = $message;
		$paste->created = time();
		$paste->topics = array();
		$paste->pastedby = $user->username;
		if($topic_id != null)
		{
			Paste::discoverTopics($db, $topic_id, $paste->topics);
		//	array_push($paste->topics, $topic_id);
		//	exit();
		}
		$db->save("pastes", $paste );
		
		$grid = $db->getGridFS();
		if(isset($_FILES["fileupload"]['name']))
		{
			$files = $_FILES["fileupload"];

			for($i = 0; $i < count($files['name']); $i++)
			{
				$file_name = $files['name'][$i];
				$file_type = $files['type'][$i];
		 		$file_size = $files['size'][$i];
		 		$file_tmp_name = $files['tmp_name'][$i];
		 		//echo $file_tmp_name;
		 		$file_error = $files['error'][$i];
		 		if( $file_error == 0) //error of 0 is good.
		 		{
		 			$extra = array(
		 					"paste_id" => $paste->_id,
		 					"type" => $file_type,
		 					"size" => $file_size,
		 					"filename" => $file_name,
		 				);
					$grid->storeFile($file_tmp_name, $extra, array( "safe" => true));
					
		 		}
				
			}
		}
		$db->close();
		//echo "saved the files, i think....";

	}      
//	else
//	{	
		Header("Location: index.php");
		exit();
//	}
?>