<?php
	include_once("../nullicon_namespace.php");
	$username = "";
	$password = "";
	$message = "";
	$username = utils::getPureString($_POST, "username", null);
	$password = utils::getPureString($_POST, "password", null);
	if($username && $password)
	{
		$db = new DB();

		$criteria = array( "username" => (string)$username);
		$user = $db->findOne("users", $criteria, new User());
		if($user->_id != null)
		{
			$hashed_pass = hash("sha512", $user->salt . $password);
			if(!strcmp($hashed_pass,  $user->password))
			{
				$_SESSION['user'] =serialize($user);
                Header("Location: index.php");
			}
			else
			{
				$message = "Invalid username or password";
			}
		}
		else
		{
			$user = new User();
			$user->username = $username;
			$user->salt = utils::randString(20);
            $user->password = hash("sha512", $user->salt . $password1);
            $user->styling = null;
            $db->save("users", $user );
            $_SESSION['user'] = serialize($user);
            Header("Location: index.php");
		}
	}

?>



<html lang="en"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"></link>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/jquery/1.8.2/jquery.min.js"></script>
        <style type="text/css">
            <?php
                    echo "
                        html, body{
                            background-color: #3b3b3b;
                            font-size: 16px;
                            color: Green;
                            font-family: Monaco,Menlo,Consolas,'Courier New',monospace;
                        }

                        #pastes, #paste-form{
                            background-color: #272727;
                        }

                        .paste{
                            background-color: #141414;
                            margin-top: 15px;
                            padding: 15px;
                            word-wrap: break-word;
                        }

                        .paste small{
                            font-size: .7em;
                            color: #767676;
                        }

                    

                        .paste pre{
                            font-size: inherit;
                            color: inherit;
                            background-color: inherit;
                            padding: 0;
                            margin: 0;
                        }

                        .paste-input{
                            background-color: #141414;
                            color: Green;
                            width: 98.5%;
                            min-height: 250px;
                            font-size: 21px;
                            border:none;
                        }

                        input.paste-input{
                        	background-color: #141414;
                            color: Green;
                            width: 98.5%;
                            height: 30px;
                            font-size: 110px;
                            border:none;

                        }

                        .paste-input:focus, input.paste-input:focus{
                            border: none;
                            outline: none;
                            box-shadow: none;

                        }

                        .paste-button{
                            width: 100% !important;
                            font-size: 55px;
                            //background-color: #272727 !important;
                            color: Green !important;
                        }

                        abbr[title]{
                            border: none;
                        }

                        .error{
                        	font-size: 40px;
                        	text-align: center;
                        	color: Orange;
                        }

                        ";
            ?>
        </style>
    </head>
    <body>
    	<div id="content" class="container">
    		 <div id="paste-form" class="span12">
                <?php
			        if($message)echo "<div class='error paste'>$message</div>";
			        echo "<form action='login.php' method='post' class='paste'>
			                <input type='text' class='paste-input' placeholder='Username' value='$username' name='username'/>
			                <input type='password' class='paste-input' placeholder='Password' name='password'/>
			                <input type='submit' class='btn paste-button' value='Login / Register'/>
			            </form>";
			        ?>
            </div>


    	</div>
    </body>
    <?php include("../footer.php"); ?>
</html>