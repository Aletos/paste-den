<?php
    include_once("../nullicon_namespace.php");
    $user = utils::getLogin();
    $page = utils::getPureString($_GET, 'page', 1);
    $nextpage = $page + 1;
    $pagesize = utils::getPureString($_GET, 'pagesize', 50);
    $topic_id = utils::getPureString($_GET, 'topic_id', null);

    $db = new DB();

    $topic = new Topic();
    if($topic_id != null)
        $topic = $db->findByID("topics", $topic_id, $topic);

    $criteria = array();
    if($topic->_id != null)
    {
        $tid = new MongoId($topic->_id);
        $criteria['topics'] = $tid;
    }
    $search = "";
    $esearch = "";
    if(isset($_GET['search']))
    {
        $search = (string)$_GET['search'];
        $esearch = utils::enchanceSearch($search);
        $criteria['message'] =  new MongoRegex("/$esearch/i"); 
    }

    $sort_criteria = array("created" => -1);  //sort by created time, -1 is for descending order
    $skip = ($page-1)*$pagesize;
    $limit = $pagesize;
    $pastes = $db->getList("pastes", $criteria, "Paste", $sort_criteria, $skip, $limit);
    $grid = $db->getGridFS();

   // if(isset($_GET['search']))
   // {
   //      $paste_set = array();
   //      foreach($pastes as $paste_tmp)
   //      {
   //          $paste_set[$paste_tmp->_id.""] = $paste_tmp;

   //      }
   //      unset($paste_tmp);

   //      //Find matching files
   //      $cursor = $grid->find(array("filename" => new MongoRegex("/$esearch/i")));
   //      while($matched_file = $cursor->getNext())
   //      {
   //          $paste_tmp = new Paste();
   //          $pid =  $matched_file->file['paste_id']."";
   //          $paste_tmp = $db->findByID("pastes", $pid, $paste_tmp);
   //          $paste_set[$paste_tmp->_id.""] = $paste_tmp;
   //      }
   //      unset($matched_file);

   //      $topics_tmp = $db->getList("topics", array("title" => new MongoRegex("/$esearch/i")), "Topic");
   //     // echo count($topics_tmp);
   //      foreach($topics_tmp as $topic_tmp)
   //      {
   //          $tid =  $topic_tmp->_id;
   //          $pastes_tmp = $db->getList("pastes", array("topics" => $tid), "Paste");
   //          foreach($pastes_tmp as $paste_tmp)
   //          {
   //              $paste_set[$paste_tmp->_id.""] = $paste_tmp;
   //          }
   //          //$paste_tmp = new Paste();
   //          //$paste_tmp = $db("pastes", $pid, $paste_tmp);
            
   //          //$paste_set[$paste_tmp->_id.""] = $paste_tmp;
   //      }

   //      unset($paste_id);
   //      $pastes = array_values($paste_set);
   //      function cmp($paste1, $paste2)
   //      {
   //          return $paste2->created - $paste1->created;
   //      }
   //      usort($pastes, "cmp");
   //      $pastes = array_splice($pastes, ($page - 1) * $pagesize, $pagesize);
   // }

    

?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"></link>
        <link href="../css/prettify.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquery.infinitescroll.min.js"></script>
        <script type="text/javascript" src="../js/behaviors/manual-trigger.js"></script>
        <!--<script type="text/javascript" src="../js/jquery.filedrop.js"></script>-->
        <script type="text/javascript" src="../js/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="../js/jquery.iframe.transport.js"></script>
        <script type="text/javascript" src="../js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="../js/prettify.js"></script>
        <!--script type="text/javascript" src="../js/jquery.fileupload-fp.js"></script>
        <script type="text/javascript" src="../js/jquery.fileupload-ui.js"></script>-->
        <script type="text/javascript" src="../js/paste.js"></script>
        <style type="text/css">
            <?php
                utils::printPasteCSS($user->styling);
            ?>
        </style>
        <script type="text/javascript">
        <?php
            if(count($pastes) > 0)
                echo "var mostRecentPasteTime = ". $pastes[0]->created.";";
            else
                echo "var mostRecentPasteTime = 0;";
            echo "var topic_id = \"$topic_id\";";
            ?>
        </script>
    </head>
    <body>
        <div id="content" class="container">
            <div class="span12">
                <div class="paste">
                        <span class="brand">Paste Den</span>
                        <abbr title="Home"><a href="index.php"><i class="icon-home"></i>Home</a></abbr>
                        <abbr title="Paste"><a href="#" onClick="showPasteForm()"><i class="icon-comment"></i>Paste</a></abbr>
                        <abbr title="Search"><a href="#" onClick="showSearchPastesForm()"><i class="icon-search"></i>Search</a></abbr>
                        <abbr title="Create a Topic"><a href="#" onClick="showCreateTopicForm()"><i class="icon-tag"></i>Topic</a></abbr>
                        <abbr title="Edit Profile"><a href="#" onClick="showUserInfoForm()"><i class="icon-user" ></i><?php echo $user->username; ?></a></abbr>
                        <span class='secondary'> - </span><a href="logoff.php">Logoff</a>
                        
                        <br/>
                        <span>
                            <?php 
                                $topic_breadcrumbs = array();
                                Topic::breadCrumbs($db, $topic_id, $topic_breadcrumbs );
                                array_push($topic_breadcrumbs, "<a class='topic' href='index.php'>Everything</a>");
                                $topic_breadcrumbs = array_reverse($topic_breadcrumbs);

                                $first = true;
                                echo "<h4>";
                                foreach($topic_breadcrumbs as $breadcrumb)
                                {
                                    if(!$first)
                                        echo "<span class='secondary'> > </span>";
                                    $first = false;
                                    echo "$breadcrumb";
                                }
                                unset($breadcrumb);
                                echo "</h4>";

                            ?>
                        </span>
                        <?php
                            echo "<p>";
                            $children = $topic->getChildren($db);
                            if(count($children) > 0)
                                echo "<span class='secondary'>Subtopics: </span>";
                            $first = true;
                            foreach($children as $child)
                            {
                                if(!$first)
                                    echo "<span class='secondary'> | </span>";
                                $first = false;
                                echo $child->getLink("index.php");
                            }
                            unset($child);
                            echo "</p>";
                        ?>             
                        <div><?php if($search) echo "Search:";?> <span id="searchTerm"><?php echo htmlspecialchars($search); ?></span></div>
                                
                </div>
            </div>
            <div id="paste-form" class="span12">
                <form action="paste.php" method="POST" class="paste" enctype="multipart/form-data" id="upload-form">
                    <input type="hidden" id="topic_id" name="topic_id" value=<?php echo $topic->_id?>></input>
                    <textarea name="message" id="message" class='paste-input' placeholder='Paste here'></textarea>
                    <input type="file" name="fileupload[]" multiple id="paste-file-field" />
                    <p id="fileQueue"></p>
                    <input type="button" id="paste-submit" value="Paste" class="btn paste-button" onClick="pasteSubmit()"></input>

                </form>
                <form action='create_topic.php'  method='POST' class='paste' id='create-topic-form'>
                    <input type="hidden" name="parent_id" value=<?php echo $topic->_id?>></input>
                    <textarea name='title' id='title' id='title' class='paste-input' placeholder='Enter Topic Title'></textarea>
                    <input type='submit' id='topic-submit' value='Create Topic' class='btn paste-button'></input>
                </form>
                <form action='index.php'  method='GET' class='paste' id='search-pastes-form'>
                    <input type="hidden" name="topic_id" value=<?php echo $topic->_id?>></input>
                    <textarea name='search' id='search' class='paste-input' placeholder='What are you looking for?'></textarea>
                    <input type='submit' id='search-submit' value='Search' class='btn paste-button'></input>
                </form>
                <?php
                $user_styling = $user->styling;
                echo "<form action='update_user.php'  method='POST' class='paste' id='user-info-form'>
                    <label for='first_name' class='secondary'>First Name:</label>
                    <input type='text' name='first_name' class='paste-input' placeholder='What is your first name?' value='$user->first_name'></input>
                    <label for='last_name' class='secondary'>Last Name:</label>
                    <input type='text' name='last_name' class='paste-input' placeholder='What is your last name?' value='$user->last_name'></input>
                    <h3 class='secondary'>Custom Sytling</h3>
                    <label for='body_color' class='secondary'>HTML Body Color:</label>
                    <input type='text' name='body_color' class='paste-input' placeholder='Hex Value' value='$user_styling[body_color]''></input>
                    <label for='body_color_secondary' class='secondary'>HTML Body Color Secondary:</label>
                    <input type='text' name='body_color_secondary' class='paste-input' placeholder='Hex Value' value='$user_styling[body_color_secondary]''></input>
                    <label for='paste_color' class='secondary'>Paste Color:</label>
                    <input type='text' name='paste_color' class='paste-input' placeholder='Hex Value' value='$user_styling[paste_color]''></input>
                    <label for='font_color' class='secondary'>Font Color:</label>
                    <input type='text' name='font_color' class='paste-input' placeholder='Hex Value' value='$user_styling[font_color]''></input>
                    <label for='font_color_secondary' class='secondary'>Secondary Font Color:</label>
                    <input type='text' name='font_color_secondary' class='paste-input' placeholder='Hex Value' value='$user_styling[font_color_secondary]''></input>
                    <label for='link_color' class='secondary'>Link Color:</label>
                    <input type='text' name='link_color' class='paste-input' placeholder='Hex Value' value='$user_styling[link_color]''></input>
                    <label for='file_color' class='secondary'>File Color:</label>
                    <input type='text' name='file_color' class='paste-input' placeholder='Hex Value' value='$user_styling[file_color]''></input>
                    <label for='topic_color' class='secondary'>Topic Color:</label>
                    <input type='text' name='topic_color' class='paste-input' placeholder='Hex Value' value='$user_styling[topic_color]''></input>
                   
                    <input type='submit' id='profile-submit' value='Save Changes' class='btn paste-button'></input>
                </form>";
                ?>

            </div>
            <div id="pastes" class="span12">
                <?php
                    foreach($pastes as $paste)
                    {
                        $created_date = date('H:i:s Y-m-d', $paste->created);
                        $regarding = "<a class='topic' href='index.php'>Everything</a>";
                        if(count($paste->topics) > 0)
                        {
                            $topic_ids = array_reverse($paste->topics);
                            foreach($topic_ids as $paste_topic_id)
                            {
                               // $paste_topic_id = $paste->topics[0];
                                $regarding .= " > ";
                                $first = false;
                                $paste_topic = new Topic();
                                $paste_topic = $db->findByID("topics", $paste_topic_id, $paste_topic);
                                $regarding .= $paste_topic->getLink("index.php");
                            }
                            unset($paste_topic_id);
                        }

                        $message_dom_id = "message_".$paste->_id;
                        $message_edit_form_id = "message_edit_".$paste->_id;
                        $by = "Pasted ";
                        if(isset($paste->pastedby))
                            $by .= "by <span class='primary'>$paste->pastedby</span>";

                        $edit = "";
                        if(!strcmp($paste->pastedby, $user->username))
                            $edit = "| <a href='#$message_edit_form_id' onClick='getPasteEditForm(\"$message_edit_form_id\", \"$message_dom_id \")'>
                            edit</a> | <a href='deletePaste.php?paste_id=$paste->_id'>delete</a>";

                        $nonreplaced_message = htmlspecialchars($paste->message);
                       // $message = nl2br($paste->message);
                        $message = htmlspecialchars($paste->message);

                        


                        $message = preg_replace(array(
                            "/&lt;code&gt;\s*(.*?)\s*&lt;\/code&gt;/s",    //matches for inserting code  {{code}}
                           //"/\[code\](.*)\[code\]/s",
                            "/\s*\[\[\s*(https:\/\/.+?)\s*\|\s*(.+?)\s*\]\]\s*/",  //match [[ https://link | title ]] 
                            "/\[\[\s*(http:\/\/.+?)\s*\|\s*(.+?)\s*\]\]/",   //match [[ http://link | title ]] 
                            "/\[\[\s*(.+?)\s*\|\s*(.+?)\s*\]\]/",  //match  [[ link | title]] then prepends http:// to the link
                            "/\[\[\s*(https:\/\/.+?)\s*\]\]/",  //match [[ https://link]] 
                            "/\[\[\s*(http:\/\/.+?)\s*\]\]/",   //match [[ http://link]] 
                            "/\[\[\s*(.+?)\s*\]\]/",  //match  [[ link]] then prepends http:// to the link
                            "/(http:\/\/.+)\s*?/",  //match just http
                            "/(https:\/\/.+)\s*?/",  //match just https

                        ),
                        array(
                             "<pre class='prettyprint'><code>\\1</code></pre>",
                            "<a href=\"\\1\">\\2</a>",
                            "<a href=\"\\1\">\\2</a>",
                             "<a href=\"http://\\1\">\\2</a>",
                             "<a href=\"\\1\">\\1</a>",
                            "<a href=\"\\1\">\\1</a>",
                             "<a href=\"http://\\1\">\\1</a>",
                              "<a href=\"\\1\">\\1</a>",
                               "<a href=\"\\1\">\\1</a>",
                            ),
                         $message);
                        
                      //  $message = escape($message);
                        echo "<div class='paste'>
                                <p id='$message_dom_id'><pre>$message</pre></p>";
                                
                        echo "<p>";
                        $gridCursor = $grid->find(array("paste_id" => $paste->_id));
                        $first = true;
                        while($gridFile = $gridCursor->getNext())
                        {
                            if(!$first)
                                echo "<span class='file'>, </span>";
                            else
                                echo "<i class='icon-file'></i> ";
                            $first = false;
                            echo sprintf("<a class='file' href='%s'>%s</a>", "download.php?id=".$gridFile->file['_id'] , $gridFile->getFilename());
                        }
                        echo "</p>";
                        echo "<small>$by at $created_date Regarding $regarding $edit</small><br/>";
    //                     echo "<pre class='prettyprint'><code> 
    // public class Main{
    //     public static void main(String [] args)
    //     {
    //         Scanner in = new Scanner(System.in);
    //     }
    // }
    //                     </code></pre>";
                        echo "</div>";
                        if($edit)
                        echo "<div id='$message_edit_form_id'  style='display:none;'><form action='editPaste.php'   method='POST' class='paste' >
                            <input type='hidden' name='paste_id' value='$paste->_id'></input>
                            <textarea name='message' class='paste-input' placeholder='Paste here'>$nonreplaced_message</textarea>
                            <input type='submit' value='Save Changes' class='btn paste-button'></input>
                        </form></div>";
                    }   
                    unset($paste);
                    echo "<a id='nextpage' href='index.php?page=$nextpage&pagesize=$pagesize";
                    if($topic_id != null) echo "&topic_id=$topic->_id";
                    if(isset($search)) echo "&search=$search";
                    echo "'>More</a>";
                ?>

            </div>

        </div>
    </body>
</html>

<?php $db->close(); ?>