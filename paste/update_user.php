<?php
    include_once("../nullicon_namespace.php");
    $user = utils::getLogin();
    $first_name = utils::getPureString($_POST, "first_name", null);
	$last_name = utils::getPureString($_POST, "last_name", null);
	$body_color = utils::getPureString($_POST, "body_color", null);
	$body_color_secondary = utils::getPureString($_POST, "body_color_secondary", null);
	$paste_color = utils::getPureString($_POST, "paste_color", null);
	$font_color = utils::getPureString($_POST, "font_color", null);
	$link_color = utils::getPureString($_POST, "link_color", null);
	$file_color = utils::getPureString($_POST, "file_color", null);
	$topic_color = utils::getPureString($_POST, "topic_color", null);
	$font_color_secondary = utils::getPureString($_POST, "font_color_secondary", null);
	$styling = array(
		"body_color" => null,
		"body_color_secondary" => null,
		"paste_color" => null,
		"font_color" => null,
		"link_color" => null,
		"file_color" => null,
		"topic_color" => null,
		"font_color_secondary" => null,
		);
	$hexcolor_regex = "/#\w+/";
	if($first_name)
		$user->first_name = $first_name;
	if($last_name)
		$user->last_name = $last_name;
	if($body_color && preg_match($hexcolor_regex, $body_color))
		$styling['body_color'] = $body_color;
	if($body_color_secondary && preg_match($hexcolor_regex, $body_color_secondary))
		$styling['body_color_secondary'] = $body_color_secondary;
	if($paste_color && preg_match($hexcolor_regex, $paste_color))
		$styling['paste_color'] = $paste_color;
	if($font_color && preg_match($hexcolor_regex, $font_color))
		$styling['font_color'] = $font_color;
	if($link_color && preg_match($hexcolor_regex, $link_color))
		$styling['link_color'] = $link_color;

	if($file_color && preg_match($hexcolor_regex, $file_color))
		$styling['file_color'] = $file_color;
	if($topic_color && preg_match($hexcolor_regex, $topic_color))
		$styling['topic_color'] = $topic_color;

	if($font_color_secondary && preg_match($hexcolor_regex, $font_color_secondary))
		$styling['font_color_secondary'] = $font_color_secondary;

	if(count($styling) > 0)
		$user->styling = $styling;
	$db = new DB();
	$db->save("users", $user );
	$db->close();
	$_SESSION['user'] = serialize($user);
	Header("Location: index.php");
	exit();
?>