<?php
    include_once("../nullicon_namespace.php");
    $user = utils::getLogin();
	$response = array("mostRecentPasteTime" => 0, "count" => -12, "pastes" => "");
	if(isset($_GET['mostRecentPasteTime']))
	{

		$response["mostRecentPasteTime"] = $_GET['mostRecentPasteTime'];
		include_once("../nullicon_namespace.php");
		$db = new DB();

		$topic_id = utils::getPureString($_GET, 'topic_id', null);

		$criteria = array(
				"created" => array( '$gt' => intval($_GET['mostRecentPasteTime']))// (string)$_GET['mostRecentPasteTime'])
			);
		if($topic_id != null)
		{
 			$tid = new MongoId($topic_id);
        	$criteria['topics'] = $tid;

		}
         $esearch = "";
		if(isset($_GET['search']))
	    {
	        $search = (string)$_GET['search'];
	        $esearch = utils::enchanceSearch($search);
	        $criteria['message'] =  new MongoRegex("/$esearch/i"); 
    	}
		$sort_criteria = array("created" => -1);  //sort by created time, -1 is for descending order
		$pastes = $db->getList("pastes", $criteria, "Paste", $sort_criteria);
		$response['count'] = count($pastes);
		$grid = $db->getGridFS();


       //  if(isset($_GET['search']) && strlen($esearch) > 0)
       // {
       //      $paste_set = array();
       //      foreach($pastes as $paste_tmp)
       //      {
       //          $paste_set[$paste_tmp->_id.""] = $paste_tmp;

       //      }
       //      unset($paste_tmp);

       //      //Find matching files
       //      $cursor = $grid->find(array("filename" => new MongoRegex("/$esearch/i")));
       //      while($matched_file = $cursor->getNext())
       //      {
       //          $paste_tmp = new Paste();
       //          $pid =  $matched_file->file['paste_id']."";
       //          $paste_tmp = $db->findByID("pastes", $pid, $paste_tmp);
       //          $paste_set[$paste_tmp->_id.""] = $paste_tmp;
       //      }
       //      unset($matched_file);

       //      $topics_tmp = $db->getList("topics", array("title" => new MongoRegex("/$esearch/i")), "Topic");
       //     // echo count($topics_tmp);
       //      foreach($topics_tmp as $topic_tmp)
       //      {
       //          $tid =  $topic_tmp->_id;
       //          $pastes_tmp = $db->getList("pastes", array("topics" => $tid, "created" => array('$gt' => intval($_GET['mostRecentPasteTime']))), "Paste");
       //          foreach($pastes_tmp as $paste_tmp)
       //          {
       //              $paste_set[$paste_tmp->_id.""] = $paste_tmp;
       //          }
       //          //$paste_tmp = new Paste();
       //          //$paste_tmp = $db("pastes", $pid, $paste_tmp);
                
       //          //$paste_set[$paste_tmp->_id.""] = $paste_tmp;
       //      }

       //      unset($paste_id);
       //      $pastes = array_values($paste_set);
       //      function cmp($paste1, $paste2)
       //      {
       //         // echo $paste1->created - $paste2->created . " ";
       //          return $paste2->created - $paste1->created;
       //      }
       //      usort($pastes, "cmp");
       //      //$pastes = array_splice($pastes, ($page - 1) * $pagesize, $pagesize);
       // }





		if(count($pastes) > 0)
			$response["mostRecentPasteTime"] = $pastes[0]->created;

		foreach($pastes as $paste)
        {
            $created_date = date('H:i:s Y-m-d', $paste->created);

            $message_dom_id = "message_".$paste->_id;
            $message_edit_form_id = "message_edit_".$paste->_id;
           // $message = nl2br($paste->message);
            $regarding = "<a class='topic' href='index.php'>Everything</a>";
            if(count($paste->topics) > 0)
            {
                $topic_ids = array_reverse($paste->topics);
                foreach($topic_ids as $paste_topic_id)
                {
                   // $paste_topic_id = $paste->topics[0];
                    $regarding .= " > ";
                    $first = false;
                    $paste_topic = new Topic();
                    $paste_topic = $db->findByID("topics", $paste_topic_id, $paste_topic);
                    $regarding .= $paste_topic->getLink("index.php");
                }
                unset($paste_topic_id);
            }
            $by = "Pasted ";
            if(isset($paste->pastedby))
                $by .= "by <span class='primary'>$paste->pastedby</span>";

            $edit = "";
            if(!strcmp($paste->pastedby, $user->username))
                $edit = "| <a href='#$message_edit_form_id' onClick='getPasteEditForm(\"$message_edit_form_id\", \"$message_dom_id \")'>edit</a>  | <a href='deletePaste.php?paste_id=$paste->_id'>delete</a>";

            $nonreplaced_message = htmlspecialchars($paste->message);

            $message = htmlspecialchars($paste->message);
            $message = preg_replace(array(
                            "/&lt;code&gt;\s*(.*?)\s*&lt;\/code&gt;/s",    //matches for inserting code  {{code}}
                           //"/\[code\](.*)\[code\]/s",
                            "/\s*\[\[\s*(https:\/\/.+?)\s*\|\s*(.+?)\s*\]\]\s*/",  //match [[ https://link | title ]] 
                            "/\[\[\s*(http:\/\/.+?)\s*\|\s*(.+?)\s*\]\]/",   //match [[ http://link | title ]] 
                            "/\[\[\s*(.+?)\s*\|\s*(.+?)\s*\]\]/",  //match  [[ link | title]] then prepends http:// to the link
                            "/\[\[\s*(https:\/\/.+?)\s*\]\]/",  //match [[ https://link]] 
                            "/\[\[\s*(http:\/\/.+?)\s*\]\]/",   //match [[ http://link]] 
                            "/\[\[\s*(.+?)\s*\]\]/",  //match  [[ link]] then prepends http:// to the link

                        ),
                        array(
                             "<pre class='prettyprint'><code>\\1</code></pre>",
                            "<a href=\"\\1\">\\2</a>",
                            "<a href=\"\\1\">\\2</a>",
                             "<a href=\"http://\\1\">\\2</a>",
                             "<a href=\"\\1\">\\1</a>",
                            "<a href=\"\\1\">\\1</a>",
                             "<a href=\"http://\\1\">\\1</a>",
                            ),
                         $message);
          //  $message = escape($message);
            $response['html'] .= "<div class='paste'>
                    <p id='$message_dom_id'><pre>$message</pre></p>";
                    
            $response['html'] .=  "<p>";
            $gridCursor = $grid->find(array("paste_id" => $paste->_id));
            $first = true;
            while($gridFile = $gridCursor->getNext())
            {
                if(!$first)
                    $response['html'] .=  ", ";
                $first = false;
                $response['html']  .=  sprintf("<a class='file' href='%s'>%s</a>", "download.php?id=".$gridFile->file['_id'] , $gridFile->getFilename());
            }
            $response['html']  .=  "</p>";
            $response['html']  .=  "<small>$by $created_date Regarding $regarding $edit</small><br/>";
            $response['html']  .=  "</div>";
            if($edit)
            $response['html']  .= "<div id='$message_edit_form_id' class='hide_this' style='display:none;'><form action='editPaste.php'   method='POST' class='paste' >
                            <input type='hidden' name='paste_id' value='$paste->_id'></input>
                            <textarea name='message' class='paste-input' placeholder='Paste here'>$nonreplaced_message</textarea>
                            <input type='submit' value='Save Changes' class='btn paste-button'></input>
                        </form></div>";
          // array_push($response['pastes'], $paste_string);
            //$response[] = $paste_string;
        }
        $db->close();
	}
	Header('Content-type: application/json');
	echo json_encode($response);
?>