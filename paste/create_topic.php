<?php
	include_once("../nullicon_namespace.php");
	$db = new DB();
	//$message = utils::getPureString($_POST, 'message');
	$title = $_POST['title'];
	$parent_id = $_POST['parent_id'];
	if(strlen($parent_id) <= 0)
		$parent_id = null;
	if($title)
	{
		$topic = new Topic();
		$topic->title = $title;
		$topic->parent_id = $parent_id;
		$db->save("topics", $topic );
		Header("Location: index.php?topic_id=$topic->_id");
		exit();
	}
	$db->close();
	Header("Location: index.php");
	exit();
?>