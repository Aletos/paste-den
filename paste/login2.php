<?php
	include_once("../nullicon_namespace.php");
	$username = "";
	$password = "";
	$message = "";
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		$db = new DB();
		$username = utils::getPureString($_POST, "username");
		$password = utils::getPureString($_POST, "password");
		$criteria = array( "username" => (string)$username);
		$user = $db->findOne("users", $criteria, new User());
		if($user->_id != null)
		{
			$hashed_pass = hash("sha512", $user->salt . $password);
			if(!strcmp($hashed_pass,  $user->password))
			{
				$_SESSION['user_id'] = $user->_id;
                Header("Location: list.php");
			}
			else
			{
				$message = "Invalid username or password";
			}
		}
		else
			$message = "Invalid username or password";
	}

?>



<html>
    <?php include("../head.php"); ?>
    <body>
        <?php
        echo $message . "<br/>";
        echo "<form action='login.php' method='post'>
                <input type='text' placeholder='Username' value='$username' name='username'/>
                <input type='password' placeholder='Password' name='password'/>
                <input type='submit' value='Login'/>
            </form>";
        ?>
    </body>
    <?php include("../footer.php"); ?>
</html>