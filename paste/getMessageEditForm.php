<?php
    include_once("../nullicon_namespace.php");
    $user = utils::getLogin();

    
    if(isset($_POST['paste_id']))
    {
    	$paste_id = $_POST['paste_id'];
    	$db = new DB();
    	$paste = new Paste();
    	$paste = $db->findByID("pastes", $paste_id, $paste);
    	if(isset($paste->_id))
    	{
    		$message = htmlspecialchars($paste->message);
    		echo "<form action='editPaste.php'  method='POST' class='paste'>
    				<input type='hidden' name='paste_id' value='$paste_id'></input>
    				<textarea name='message' class='paste-input' placeholder='Paste here' value='$message'></textarea>
    				<input type='submit' value='Save Changes' class='btn paste-button'></input>
    			</form>";
    	}
    	$db->close();
    }
?>