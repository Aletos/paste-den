<?php
    include_once("../nullicon_namespace.php");
    $user = utils::getLogin();

    if(isset($_POST['paste_id']) && isset($_POST['message']))
    {
    	$paste_id = $_POST['paste_id'];
    	$message = $_POST['message'];
    	$paste = new Paste();
    	$db = new DB();

		$paste = $db->findByID("pastes", $paste_id, $paste);
		if(isset($paste->_id))
		{
			if(!strcmp($paste->pastedby, $user->username))
			{
				$paste->message = $message;
    			$db->save("pastes", $paste );
    			Header("Location: " . $_SERVER['HTTP_REFERER']);
    		}
    	}
    	else
    		echo "why";
    	$db->close();
    }

?>