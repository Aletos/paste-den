<?php
    include_once("../nullicon_namespace.php");
    $body = "";
    if(isset($_GET['user_id']))
    {
    	$user_id = utils::getPureString($_GET, "user_id");
    	$db = new DB();
		$model = $db->findByID("users", $user_id, new User());
		include("../views/user/details.php");
    }
    else
    	echo "No user id is set";

?>
