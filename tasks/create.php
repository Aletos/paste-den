<?php
	include_once("../nullicon_namespace.php");
	$user_id = utils::getLogin();

	$title = null;

	if(isset($_POST['title']))
	{
		$title = utils::getPureString($_POST, 'title');
		if(strlen($title) > 0)
		{
			$db = new DB();
			$task = new Task();
			$task->title = $title;
			$task->user_id = $user_id;
			$task->created = utils::getCurrentTimestamp();
			$task->is_recurring = false;
			$task->is_completed = false;
			$db->save("tasks", $task );
		}
	}
?>

<html>
    <?php include("../head.php"); ?>
    <body>
        <?php
        echo $message . "<br/>";
        echo "<form action='create.php' method='post'>
                <input type='text' placeholder='Task Title' value='$title' name='title'/>
                <input type='submit' value='Create'/>
            </form>";
        ?>
    </body>
    <?php include("../footer.php"); ?>
</html>