<?php
    class UserView{
        var $user;

        function UserView($user)
        {
            $this->user = $user;
        }

        public function details()
        {
            return "Details view for ".$this->user->username;
        }

    }
?>
