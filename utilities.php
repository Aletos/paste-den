<?php

	class utils{
		public static function getPureString($container, $key, $default=null)
		{
			if(isset($container[$key]))
			{
				$config = HTMLPurifier_Config::createDefault();
				$purifier = new HTMLPurifier($config);
				return $purifier->purify($container[$key]);
			}
			else
				return $default;

		}

		public static function purifyString($s)
		{
			$config = HTMLPurifier_Config::createDefault();
			$purifier = new HTMLPurifier($config);
			return $purifier->purify($s);
		}

		public static function getCurrentTimestamp()
		{
			return gmdate('Y-m-d H:i:s');
		}

		public static function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
		{
		    $str = '';
		    $count = strlen($charset);
		    while ($length--) {
		        $str .= $charset[mt_rand(0, $count-1)];
		    }
		    return $str;
		}

		//Gets the logged in user id from the session.
		//If this isn't set then it redirects the login page.
		public static function getLogin()
		{
			if(isset($_SESSION['user']))
			{
				return unserialize($_SESSION['user']);
			}
			else
			{
				Header( sprintf ("Location: %s", "http://". $_SERVER['SERVER_NAME']. "/nullicon/paste/login.php") );
				exit();
			}

		}

		public static function prepareLink($link)
		{
			if(!self::startsWith($link, "http://") && !self::startsWith($link, "https://"))
				$link = "http://".$link;
			echo $link;
			return $link;
		}

		public static function startsWith($haystack, $needle)
		{
		    $length = strlen($needle);
		    return (substr($haystack, 0, $length) === $needle);
		}

		public static function enchanceSearch($search)
		{
			return preg_replace("/\s+/", ".*", $search);
		}

        public static function time_since($since) {
            $chunks = array(
                array(60 * 60 * 24 * 365 , 'year'),
                array(60 * 60 * 24 * 30 , 'month'),
                array(60 * 60 * 24 * 7, 'week'),
                array(60 * 60 * 24 , 'day'),
                array(60 * 60 , 'hour'),
                array(60 , 'minute'),
                array(1 , 'second')
            );

            for ($i = 0, $j = count($chunks); $i < $j; $i++) {
                $seconds = $chunks[$i][0];
                $name = $chunks[$i][1];
                if (($count = floor($since / $seconds)) != 0) {
                    break;
                }
            }

            $print = ($count == 1) ? '1 '.$name : "$count {$name}s";
            return $print;
        }


		public static function printPasteCSS($style)
		{
			$body_color = isset($style['body_color']) ? $style['body_color']: "#3b3b3b";
			$body_color_secondary = isset($style['body_color_secondary']) ? $style['body_color_secondary']: "#272727";
			$paste_color = isset($style['paste_color']) ? $style['paste_color']: "#141414";
			$font_color = isset($style['font_color']) ? $style['font_color']: "Green";
			$font_color_secondary = isset($style['font_color_secondary']) ? $style['font_color_secondary']: "#767676";
			$link_color = isset($style['link_color']) ? $style['link_color']: "#0088cc";
			$file_color = isset($style['file_color']) ? $style['file_color']: "#cc6600";
			$topic_color = isset($style['topic_color']) ? $style['topic_color']: "#b3b300";
			//echo isset($style['body_color']);
			echo "
                        html, body{
                            background-color: $body_color;
                            font-size: 16px;
                            color: $font_color;
                            font-family: Monaco,Menlo,Consolas,'Courier New',monospace;
                        }

                        #pastes, #paste-form{
                            background-color: $body_color_secondary;
                        }

                        .paste{
                            background-color: $paste_color;
                            margin-top: 15px;
                            padding: 15px;
                            word-wrap: break-word;
                        }

                        .paste small{
                            font-size: .7em;
                            color: $font_color_secondary;
                        }

                    

                        .paste pre{
                            font-size: inherit;
                            color: inherit;
                            background-color: inherit;
                            padding: 0;
                            margin: 0;
                            border: none;
                        }

                        .paste-input{
                            background-color: $paste_color;
                            color: $font_color;
                            width: 98.5%;
                            min-height: 250px;
                            font-size: 21px;
                            border:none;
                            box-shadow: none;
                        }
                        .paste-input:focus{
                            border: none;
                            outline: none;
                            box-shadow: none;

                        }

                        input.paste-input{
                            background-color: $paste_color;
                            color: $font_color;
                            width: 98.5%;
                            min-height: 0px;
                            font-size: 21px;
                            border:none;
                             box-shadow: none;
                        }
                        input.paste-input:focus{
                            border: none;
                            outline: none;
                            box-shadow: none;

                        }

                        .topic{
                        	color: $topic_color;
                        }

                        a.topic:hover{
                        	color: $topic_color;
                        }

                        a{
                        	color: $link_color;
                        }
                        a:hover{
                        	color: $link_color;
                        }

                        .file{
                        	color: $file_color;
                        }

           				a.file:hover{
           					color: $file_color;
           				}

                        .paste-button{
                            width: 100% !important;
                            font-size: 21px;
                            //background-color: #272727 !important;
                            color: $font_color !important;
                        }

                        pre.prettyprint{
                            background-color: White;
                            font-size: 8px;
                            padding: 10px;
                        }

                        .primary{
                            color: $font_color;
                        }

                        .secondary{
                        	color: $font_color_secondary;
                        }

                        abbr[title]{
                            border: none;
                        }

                        textarea::-webkit-input-placeholder, input::-webkit-input-placeholder {
						    color: $font_color_secondary;
						}
						textarea:-moz-placeholder, input:-moz-placeholder  {
						    color:    $font_color_secondary;
						}
						textarea:-ms-input-placeholder, input:-ms-input-placeholder{
						        color:    $font_color_secondary;
						}

                        .brand{
                            font-size: 1.5em;

                        }

                        ";
		}
	}

?>