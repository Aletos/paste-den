var pasteFiles;
function pasteSubmit()
{
	if(pasteFiles)
	{
		pasteFiles.submit();
	}
	else
	{
		noFileSubmit();
		//$("#upload-form").submit();
	}
}

function pasteFormClear()
{
	pasteFiles = null;
	$("#fileQueue").html("");
	$("#message").val("");
	$("#paste-submit").slideUp();
}

function noFileSubmit()
{
	var value = $("#message").val();
	var topic_id = $("#upload-form #topic_id").val();
	if(value)
	{
		var data = { "message" : value, "topic_id" : topic_id};
		$.ajax({
			  url: 'paste.php',
			  data : data,
			  type : "POST",
			  success: function(data) {
			  	pasteFormClear();
			  	pastePoll();
			  }
			});
	}
	
}

function pastePoll()
{
	var searchTerm = $("#searchTerm").html();
	var data = { "mostRecentPasteTime" : mostRecentPasteTime, "topic_id" : topic_id,
		"search" : searchTerm
	  };
	$.ajax({
		  url: 'pastepoll.php',
		  context: $("#pastes"),
		  dataType : "json",
		  data : data,
		  success: function(data) {
		  	//alert(data['count']);
		  	if(data['count'] > 0)
		  	{	

		  		$(data['html']).hide().prependTo("#pastes").slideDown("slow");
		  		$(".hide_this").hide();
		  		
		  		mostRecentPasteTime = data['mostRecentPasteTime'];
		  		prettyPrint();
		  	//	alert(mostRecentPasteTime);
		  	}
		  }
		});
}

var pasteForm;
var createTopicForm;
var currentForm;
var searchPastesForm;
var userInfoForm;

function showCreateTopicForm()
{
	currentForm.hide();
	createTopicForm.show();
	currentForm = createTopicForm;
}

function showPasteForm()
{
	currentForm.hide();
	pasteForm.show();
	currentForm = pasteForm;
}

function showPasteForm()
{
	currentForm.hide();
	pasteForm.show();
	currentForm = pasteForm;
}

function showSearchPastesForm()
{
	currentForm.hide();
	searchPastesForm.show();
	currentForm = searchPastesForm;
}

function showUserInfoForm()
{
	currentForm.hide();
	userInfoForm.show();
	currentForm = userInfoForm;
}

$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function getPasteEditForm(edit_id, dom_id)
{
	$("#"+dom_id).html($("#"+edit_id).html());
}


$(document).ready(function (){
		pasteForm = $("#upload-form");
		currentForm = pasteForm;
		createTopicForm = $("#create-topic-form");
		createTopicForm.hide();
		searchPastesForm = $("#search-pastes-form");
		searchPastesForm.hide();
		userInfoForm = $("#user-info-form");
		userInfoForm.hide();

		//dropdown
		//$('.dropdown-toggle').dropdown();


		//pretty print
		prettyPrint();

		//infinte scrolling code
		$('#pastes').infinitescroll({
		navSelector  	: "a#nextpage:last",
		nextSelector 	: "a#nextpage:last",
		itemSelector 	: "#pastes div",
		dataType	 	: 'html',
		loading: {
			finishedMsg: "<div class='paste'>No more pastes available</div>",
			msgText: "<div class='paste'>Loading more pastes</div>",
			img: "",
		}
		});

		//Javascript for dynamic paste button.
		$("#paste-submit").hide();
		$("#message").keyup(function(event){

			var button = $("#paste-submit");
			var message = $("#message").val();
			
			if(message)
				button.slideDown();
			else
				button.slideUp();
		});
		$("#search-submit").hide();
		$("#search").keyup(function(event){

			var button = $("#search-submit");
			var message = $(this).val();
			
			if(message)
				button.slideDown();
			else
				button.slideUp();
		});

		$("#topic-submit").hide();
		$("#title").keyup(function(event){

			var button = $("#topic-submit");
			var message = $(this).val();
			
			if(message)
				button.slideDown();
			else
				button.slideUp();
		});

		$("#message").keydown(function(event){

			var message = $("#message");
			if(event.which == 9)
			{
				event.preventDefault();
				var cursorPosition = $("#message").prop("selectionStart");
				var value = message.val();
				value = value.substring(0, cursorPosition) + "\t" + value.substring(cursorPosition);
				message.val(value);
				$("#message").selectRange(cursorPosition+1, cursorPosition+1);
				return false;
			}
		});
		
		$("#paste-file-field").hide();
		$("#paste-form").fileupload({
			singleFileUploads : false,
			limitMultiFileUploads: 100,
			formData : function (form) {
			    return form.serializeArray();
			},
			add : function(e, data){
				pasteFiles = data;
				var ele = $("#fileQueue");
				var first = true;
				ele.html("");
				 $.each(data.files, function (index, file) {
				 	if(!first)
				 		ele.append(", ");
				 	first = false;
			        ele.append(file.name);
			    });

				$("#paste-submit").slideDown();
			},
			dragover : function ()
			{
				
			},
			send : function()
			{

			},
			done : function()
			{
				pasteFormClear();
				pastePoll();
			}
		});

		
		//Javascript for paste polling
		var pollingInterval = 8 * 1000; //5 seconds
		setInterval(function ()
		{
			pastePoll();
		}, pollingInterval);
		

		

		
});