<?php
    class Paste{

        var $_id;
        var $message;
        var $created;
        var $topics;
        var $pastedby;
        
        function Paste(){

        }

        public static function discoverTopics($db, $topic_id, &$topics)
        {
            
            $topic = new Topic();
            $topic = $db->findByID("topics", $topic_id, $topic);
            if($topic->_id != null)
            {
                array_push($topics, $topic->_id);
                Paste::discoverTopics($db, $topic->parent_id, $topics);
            }
        }

        public static function compare($paste1, $paste2)
        {
            return $paste1->created - $paste2->created;
        }
    }
?>
