<?php
    class Topic{

        var $_id;
        var $title;
        var $parent_id;

        
        function Topic(){

        }

        public function getParent($db)
        {
            if(isset($this->parent_id))
            {
                $parent = new Topic();
                return $db->findByID("topics", $this->parent_id, $parent);
            }
            return null;
        }

        public function getChildren($db)
        {
            $id = null;
            if($this->_id != null)
                $id = $this->_id."";
            return $db->getList("topics", array("parent_id" => $id), "Topic");
        }

        public function getTitle()
        {
            return htmlspecialchars($this->title);
        }

        public function getLink($base_url)
        {
            if(isset($this->_id))
            {
                $title = htmlspecialchars($this->title);
                return "<a class='topic' href='$base_url?topic_id=$this->_id'>$title</a>";
            }
            else
                return "";
        }

        public static function breadCrumbs($db, $topic_id, &$topics)
        {
            
            $topic = new Topic();
            $topic = $db->findByID("topics", $topic_id, $topic);
            if($topic->_id != null)
            {
                array_push($topics, $topic->getLink());
                Topic::breadCrumbs($db, $topic->parent_id, $topics);
            }
        }

    }
?>