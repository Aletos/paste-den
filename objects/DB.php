<?php
    class DB{
        var $hostname = 'localhost';
        var $conn;
        var $db;

        function DB(){
            $this->conn = new Mongo($this->hostname);
            $this->db = $this->conn->test;
        }

        function findByID($collection_name, $id, $target)
        {
            $collection = $this->db->$collection_name;
            $criteria = array(
                '_id' => new MongoId($id),
            );
            $data = $collection->findOne($criteria);
            return $this->build($data, $target);            
        }

        function remove($collection_name, $criteria)
        {
            $collection = $this->db->$collection_name;
            $collection->remove($criteria);         
        }

        function removeByID($collection_name, $id)
        {
            $collection = $this->db->$collection_name;
            $criteria = array(
                '_id' => new MongoId($id),
            );
            $collection->remove($criteria);
        }

        function build($data, $target)
        {
            foreach($data as $key => $value)
            {
      //          if(!property_exists($target, $key))
                    $target->$key = $value;
            }
            return $target;    
        }

        function getList($collection_name, $criteria, $target_name, $sort_criteria = null, $skip=0, $limit=0)
        {
            $collection = $this->db->$collection_name;

            $data = $collection->find($criteria);
            $data->skip($skip);
            $data->limit($limit);
            if(isset($sort_criteria))$data->sort($sort_criteria);
            $objs = array();
            foreach($data as $dat)
                array_push($objs, $this->build($dat, new $target_name()));
            return $objs;
        }

        function findOne($collection_name, $criteria, $target)
        {
            $collection = $this->db->$collection_name;
            $data = $collection->findOne($criteria);
            return $this->build($data, $target); 
        }

        //Saves the object the database
        public function save($collection_name, $obj)
        {
            if(isset($obj->_id))
                $this->updateAllFields($collection_name, $obj);
            else
                $this->insert($collection_name, $obj);
        }

        public function updateAllFields($collection_name, $obj)
        {
            $item = array();
            $fields = get_object_vars($obj);
            $id;

            foreach($fields as $key => $value)
            {
                if(strcmp($key, "_id"))
                    $item[$key] = $value;
                else
                    $id = $value;
            }
            $criteria = array("_id" => $id);
            $this->db->$collection_name->update($criteria, $item); 
        }

        public function insert($collection_name, $obj)
        {
            $item = array();
            $fields = get_object_vars($obj);
            foreach($fields as $key => $value)
            {
                if(strcmp($key, "_id"))
                    $item[$key] = $value;
            }
            $this->db->$collection_name->insert($item); 
            $obj->_id = $item['_id'];

        }

        public function getGridFS()
        {
            return $this->db->getGridFS();
        }

        function close(){
            $this->conn->close();
        }
    }
?>
